# Team Members

* Arash Ajam
* Mohammad Anani


# 1st and 2nd Normal Forms
	After reading this section you'll be able to relational model need normalization and how to change it to different types of normal forms.
	The knowledge of normal forms help to prevent anomolies for deletion, update or insert operations.

# SQL Join operations
	After reading about Queries you'll be able to perform query operations on your database.
	The understanding of SQL Quries will help to implement a database design and retrieve data.
	

# Mapping ER diagram to Relational Model
	After reading this section you'll be able to map an ER diagram to relational model.
	The understanding of this would help with more advanced representaion of your database design.
	Relational model is a critical part of designing your database.
